///-----------------------------------------------------------------
/// Author : Valentin DELONNELLE
/// Date : 01/07/2019 16:06
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.ValentinDelonnelle.MetroidVania.MetroidVania.GameObject {
	public class GameObject : MonoBehaviour {

        private int life = 1;
        [SerializeField] protected int maxLife = 1;
        
        protected virtual void Init()
        {
            life = maxLife;
        }

        protected virtual void LoseLife(int amount)
        {
            life -= amount;
            if (life <= 0) Destroy(gameObject);
        }

        protected virtual void GainLife(int amount)
        {
            life += amount;
            if (life > maxLife) life = maxLife;
        }
    }
}