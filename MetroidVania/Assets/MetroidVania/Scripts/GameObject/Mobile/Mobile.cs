///-----------------------------------------------------------------
/// Author : Valentin DELONNELLE
/// Date : 01/07/2019 16:05
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.ValentinDelonnelle.MetroidVania.MetroidVania.GameObject.Mobile {
	public class Mobile : GameObject {
        [SerializeField] protected float speed = 10;
        [SerializeField] protected float gravity = 1.6f;
        private Vector3 velocity = new Vector3();
        protected Vector3 acceleration = new Vector3();
        protected Vector3 friction = new Vector3();

        [SerializeField] protected float frictionAir = 0.96f;
        [SerializeField] protected float frictionGround = 0.5f;
        [SerializeField] private float accelerationGround = 15;
        [SerializeField] private float accelerationAir = 0.75f;

        [SerializeField] private Vector3 maxSpeed = new Vector3();
        [SerializeField] private float maxHorizontalSpeed = 15;
        [SerializeField] private float maxVerticalSpeed = 26;

        private void Start()
        {
            acceleration.x = accelerationGround;
            acceleration.y = accelerationAir;
            
        }

        protected void Move()
        {
            
            velocity.x = acceleration.x;
            velocity.y = acceleration.y;

            velocity.x *= friction.x;
            velocity.y *= friction.y;
            
            //velocity.x = (velocity.x < 0 ? -1 : 1) * Mathf.Min(Mathf.Abs(velocity.x), maxHorizontalSpeed);
            //velocity.y = (velocity.y < 0 ? -1 : 1) * Mathf.Min(Mathf.Abs(velocity.y), maxVerticalSpeed);

            Debug.Log(velocity);
            transform.position += velocity * Time.deltaTime;
            acceleration.Set(0, 0, 0);
        }
	}
}