///-----------------------------------------------------------------
/// Author : Valentin DELONNELLE
/// Date : 01/07/2019 16:15
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.ValentinDelonnelle.MetroidVania.MetroidVania.GameObject.Mobile {
	public class Player : Mobile {
	    
		private void Start () {
		}
		
		private void Update () {

            //float h = Input.GetAxis("Horizontal");
            //float v = Input.GetAxis("Vertical");
            //Vector3 Velocity = new Vector3(h, v, 0);
            //Velocity = Velocity.normalized * speed * Time.deltaTime;
            //transform.position += Velocity;
            acceleration.y -= gravity;
            friction.Set(frictionAir, frictionAir,0);
            Move();
            
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Debug.Log("triger");
        }
    }
}