///-----------------------------------------------------------------
///   Author : Théo Sabattié                    
///   Date   : 24/01/2019 09:41
///-----------------------------------------------------------------

using System;
using UnityEngine;
using UnityEngine.UI;

namespace Com.IsartDigital.Common {
    [RequireComponent(typeof(Toggle)), ExecuteInEditMode]
    public class ToggleSwitchSprite : MonoBehaviour {
        [SerializeField] private Sprite spriteIsOff;
        [SerializeField] private Sprite spriteIsOn;

        private Image image;

        private void Start () {
            Toggle toggle = GetComponent<Toggle>();
            image = toggle.image;
            image.sprite = toggle.isOn ? spriteIsOn : spriteIsOff;
            toggle.onValueChanged.AddListener(Toggle_OnValueChanged);
        }

        private void Toggle_OnValueChanged(bool newValue)
        {
            image.sprite = newValue ? spriteIsOn : spriteIsOff;
        }
    }
}