using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;
[RequireComponent(typeof(Text))]
public class TextTypingMachine : MonoBehaviour {

    [SerializeField] private float promptDuration = 3f;
    [SerializeField] private bool fixedDuration = true;
    [SerializeField] private bool fix = false;

    private static float CHARACTER_PROMPT_DELAY = 0.05f;
    private Text textScript;
    private string text;
    private char[] characters;
    private int textLength;
    private int index;

    public bool isTextDone { get { return index >= textLength; } private set { } }

    private IEnumerator PromptText()
    {
        float characterPromptDelay = fixedDuration ? CHARACTER_PROMPT_DELAY : promptDuration / textLength;

        while(index < textLength)
        {
            textScript.text += characters[index];
            index++;
            yield return new WaitForSeconds(characterPromptDelay);
        }
        yield break;
    }

    public void SetText()
    {
        textScript = GetComponent<Text>();
        text = textScript.text;
        textLength = text.Length;
        characters = text.ToCharArray();
        textScript.text = "";
        if(fix)
        {
            text = "Two clients ! Time to introduce you to your other ovens.";
            textLength = text.Length;
            characters = text.ToCharArray();
        }

        index = 0;
        StopAllCoroutines();
        StartCoroutine(PromptText());
    }

    public void Skip()
    {
        StopAllCoroutines();
        index = textLength;
        textScript.text = text;
    }
}
