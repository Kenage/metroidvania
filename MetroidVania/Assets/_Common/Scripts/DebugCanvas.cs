using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugCanvas : MonoBehaviour {

    [SerializeField] private Text TextWindow;
    private static Text log;

    private void Awake()
    {
        log = TextWindow;
        EmptyLog();
    }

    public static void Log(string text)
    {
        if (log == null) return;
        log.text = log.text + "\n" + text;
    }

    public static void LogWarning(string text)
    {
        if (log == null) return;
        log.text += log.text + "\n" + "WARNING: " + text;
        log.color = Color.yellow;
    }

    public static void LogError(string text)
    {
        if (log == null) return;
        log.text += log.text + "\n" + "ERROR: " + text;
        log.color = Color.red;
    }

    public static void EmptyLog()
    {
        if (log == null) return;
        log.text = "Debug:";
    }
}
