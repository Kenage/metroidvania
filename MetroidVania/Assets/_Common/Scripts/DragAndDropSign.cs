///-----------------------------------------------------------------
/// Author : Caitlin Baltus
/// Date : 07/06/2019 11:08
///-----------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.IsartDigital
{
    public class DragAndDropSign : MonoBehaviour
    {

        [SerializeField] private List<Transform> directions = new List<Transform>();
        [SerializeField] private AnimationCurve animationCurve;

        [SerializeField] private GameObject nextAnim;
        [SerializeField, Range(1f, 200f)] private float speed = 150f;

        [SerializeField]private int target = 0;
        private bool reverse = false;
        private Vector3 nextPosition;

        private void OnEnable()
        {

            transform.GetComponent<Image>().CrossFadeAlpha(0, 0.01f, true);
            transform.position = directions[0].position;
        }
        private void Update()
        {

            if (transform.position == directions[target].position)
            {

                transform.position = directions[1].position;
                transform.GetComponent<Image>().CrossFadeAlpha(0, 0.01f, true);

                if (nextAnim != null)
                {
                    if(!nextAnim.activeSelf)nextAnim.SetActive(true);
                    else nextAnim.SetActive(false);
                }
            }
            
            nextPosition = directions[target].position;
            transform.GetComponent<Image>().CrossFadeAlpha(1, 0.5f, true);
            transform.position = Vector3.MoveTowards(transform.position, nextPosition, speed * Time.unscaledDeltaTime);

        }


    }
}