using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

    public static Action OnLeftKeyPressed;
    public static Action OnRightKeyPressed;
    public static Action OnDownKeyPressed;
    public static Action OnUpKeyPressed;
    public static Action OnSwipeRight;
    public static Action OnSwipeLeft;
    public static Action OnSwipeUp;
    public static Action OnSwipeDown;
    public static Action OnClick;
    public static Action OnStopDrag;

    private static float timeHeld;
    private static float clickThreshold = 1.5f;
    private static float swipeThreshold = 0.07f;
    private static float swipeTestDuration = 0.016f;
    private static bool hasClicked = false;
    private static Vector2 mouseStartPos;
    private static Vector2 mouseEndPos;

    private static GameObject objDrag;
    private static Vector3 objStartPos;
    private static Vector3 dragOffset;
    private static Vector2 swipeTestDirection;
    private static float swipeAngle = 45;
    private static bool isTestingSwipe = false;

    private static Controller Instance;

    private void Awake()
    {

        OnLeftKeyPressed += Void;
        OnRightKeyPressed += Void;
        OnUpKeyPressed += Void;
        OnDownKeyPressed += Void;
        OnSwipeRight += Void;
        OnSwipeLeft += Void;
        OnSwipeUp += Void;
        OnSwipeDown += Void;
        OnClick += Void;

        Instance = this;

    }
    



    private void Update()
    {
        if (objDrag != null)
        {
            DragObject();
            return;
        }

        /*if (Input.GetKeyDown(KeyCode.RightArrow)) OnRightKeyPressed();
        else if (Input.GetKeyDown(KeyCode.LeftArrow)) OnLeftKeyPressed();

        if (Input.GetKeyDown(KeyCode.UpArrow)) OnUpKeyPressed();
        else if (Input.GetKeyDown(KeyCode.DownArrow)) OnDownKeyPressed();*/

        if (Input.GetMouseButtonDown(0)) StartClick();
        else if (Input.GetMouseButtonUp(0)) ReleaseClick();
        else if (Input.GetMouseButton(0)) HoldClick();
    }

    private static void StartClick(Vector2 startPosition, float time)
    {
        mouseStartPos = startPosition;
        
        timeHeld = time;
        hasClicked = true;
    }

    private static void StartClick()
    {
        mouseStartPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

        timeHeld = 0;
        hasClicked = true;
    }

    private static void ReleaseClick()
    {
        if (!hasClicked) return;
        hasClicked = false;

        mouseEndPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        Vector2 swipeMovement = mouseEndPos - mouseStartPos;

        if (swipeMovement.x > swipeThreshold && Math.Abs(swipeMovement.x) > Math.Abs(swipeMovement.y)) OnSwipeLeft();
        else if (swipeMovement.x < -swipeThreshold && Math.Abs(swipeMovement.x) > Math.Abs(swipeMovement.y)) OnSwipeRight();
        else if (swipeMovement.y > swipeThreshold) OnSwipeDown();
        else if (swipeMovement.y < -swipeThreshold) OnSwipeUp();
        else if (timeHeld < clickThreshold) OnClick();
    }

    private static void HoldClick()
    {
        timeHeld += Time.deltaTime;
    }

    public static void StartSwipeTest(GameObject obj, Vector2 direction, float angle)
    {
        mouseStartPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        isTestingSwipe = true;
        swipeTestDirection = direction;
        swipeAngle = angle;
        swipeTestDuration = Time.deltaTime;
        StartDrag(obj);
        Instance.StopAllCoroutines();
        Instance.StartCoroutine(SwipeTestCoroutine());
    }

    private static void EndSwipeTest()
    {
        mouseEndPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        isTestingSwipe = false;

        if (Vector3.Angle(swipeTestDirection, mouseEndPos - mouseStartPos) > swipeAngle)
        {
            StopDrag();
            Vector2 swipeMovement = mouseEndPos - mouseStartPos;
            if (swipeMovement.x > swipeThreshold && Math.Abs(swipeMovement.x) > Math.Abs(swipeMovement.y)) OnSwipeLeft();
            else if (swipeMovement.x < -swipeThreshold && Math.Abs(swipeMovement.x) > Math.Abs(swipeMovement.y)) OnSwipeRight();
            else if (swipeMovement.y > swipeThreshold) OnSwipeDown();
            else if (swipeMovement.y < -swipeThreshold) OnSwipeUp();
            else StartClick(mouseStartPos, swipeTestDuration);
            
        }
    }

    private static IEnumerator SwipeTestCoroutine()
    {
        Vector2 currentPosition;
        float elapsedTime = 0;
        
        while(elapsedTime < swipeTestDuration && Input.GetMouseButton(0))
        {
            currentPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            if(currentPosition != mouseStartPos) elapsedTime += Time.deltaTime;
            yield return null;
        }
        EndSwipeTest();
        yield break;
        
    }

    public static void StartDrag(GameObject obj)
    {
        hasClicked = false;
        objStartPos = obj.transform.position;
        objDrag = obj;

        dragOffset = objDrag.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
    }

    public static void StopDrag(bool backToStartPos = false)
    {
        if (objDrag == null) return;
        else if (backToStartPos) objDrag.transform.position = objStartPos;
        objDrag = null;
        if(OnStopDrag != null) OnStopDrag();
    }

    private static void DragObject()
    {
        if (isTestingSwipe) return;
        Vector3 curScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
        objDrag.transform.position = curPosition + dragOffset;

    }

    private void Void()
    {
    }

    private void OnDestroy()
    {
        OnLeftKeyPressed -= Void;
        OnRightKeyPressed -= Void;
        OnUpKeyPressed -= Void;
        OnDownKeyPressed -= Void;
        OnSwipeRight -= Void;
        OnSwipeLeft -= Void;
        OnSwipeUp -= Void;
        OnSwipeDown -= Void;
        OnClick -= Void;
    }

}
