///-----------------------------------------------------------------
/// Author : Caitlin Baltus
/// Date : 07/06/2019 10:34
///-----------------------------------------------------------------

using System.Collections;
using UnityEngine;

namespace Com.IsartDigital {
	public class TouchSign : MonoBehaviour {

        [SerializeField] private Transform target;
        [SerializeField, Range(1f, 150f)] private float speed = 125f;
        [SerializeField] private AnimationCurve animationCurve;

        private Vector3 initPos;
        private Vector3 targetPos;

        private void OnEnable()
        {
            initPos = transform.position;
            targetPos = target.position;
            StopAllCoroutines();
            StartCoroutine(TouchSignGo());
        }
        
		
		
        private IEnumerator TouchSignGo()
        {
            StopCoroutine(TouchSignBack());

            while (transform.position != targetPos)
            {

                transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.unscaledDeltaTime);
                yield return null;
            }

            targetPos = initPos;
            initPos = transform.position;

            StartCoroutine(TouchSignBack());
        }

        private IEnumerator TouchSignBack()
        {
            StopCoroutine(TouchSignGo());
            
            while (transform.position != targetPos)
            {

                transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.unscaledDeltaTime);
                yield return null;
            }

            targetPos = target.position;
            initPos = transform.position;

            StartCoroutine(TouchSignGo());
        }



    }
}