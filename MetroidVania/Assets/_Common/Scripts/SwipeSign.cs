///-----------------------------------------------------------------
/// Author : Caitlin Baltus
/// Date : 07/06/2019 11:08
///-----------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

namespace Com.IsartDigital {
	public class SwipeSign : MonoBehaviour {

        [SerializeField] private List<Transform> directions = new List<Transform>();
        [SerializeField] private AnimationCurve animationCurve;
        [SerializeField, Range(20f, 70f)] private float speed = 45f;

        [SerializeField]private int target = 0;
        private bool reverse = false;
        private Vector3 nextPosition;

        private void Update()
        {

            if (transform.position == directions[target].position)
            {

                
                if (target == (directions.Capacity - 1)) reverse = true;


                if (reverse)
                {

                    if (target == 0)
                    {
                        reverse = false;
                        target++;

                    }
                    else target--;

                }

                else target++;



            }

            nextPosition = directions[target].position;
            transform.position = Vector3.MoveTowards(transform.position, nextPosition, animationCurve.Evaluate(speed * Time.unscaledDeltaTime));

        }


    }
}