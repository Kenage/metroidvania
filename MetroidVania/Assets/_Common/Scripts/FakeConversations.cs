///-----------------------------------------------------------------
/// Author : Caitlin Baltus
/// Date : 07/06/2019 14:31
///-----------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.IsartDigital {
	public class FakeConversations : MonoBehaviour {

        [SerializeField] private List<Sprite> conversationList;
        [SerializeField] private int WaitingTime = 5;
        [SerializeField] private int firstWaitingTime = 0;

        [SerializeField] private Image conversationContainer;
        private float eLapsedTime = 0;
        private int index = 0;
        private bool canSpeak = false;

        private void OnEnable()
        {
            index = 0;

            if (conversationContainer.gameObject.activeSelf) conversationContainer.gameObject.SetActive(false);
            canSpeak = false;
        }


        private void Update () {

            eLapsedTime += Time.unscaledDeltaTime;


            if (eLapsedTime>=firstWaitingTime)
            {
                canSpeak = true;
            }


            if (eLapsedTime >= WaitingTime && canSpeak)
            {
                eLapsedTime = 0;
                index++;
                if (index >= conversationList.Count) index = 0;

                if (!conversationContainer.gameObject.activeSelf) conversationContainer.gameObject.SetActive(true);
                conversationContainer.sprite = conversationList[index];


            }
            
            if(eLapsedTime>= WaitingTime/2)
            {

                if (conversationContainer.gameObject.activeSelf) conversationContainer.gameObject.SetActive(false);
            }
            
            
		}
	}
}